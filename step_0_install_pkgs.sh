#!/bin/bash

sudo yum -y --enablerepo=extras install epel-release

sudo yum -y group install "Development Tools"

sudo yum install -y \
git tree ipmitool autoconf libtool automake m4 re2c tclx \
coreutils graphviz patch readline-devel libXt-devel libXp-devel libXmu-devel \
libXpm-devel lesstif-devel gcc-c++ ncurses-devel perl-devel net-snmp net-snmp-utils \
net-snmp-devel libxml2-devel libpng12-devel libzip-devel libusb-devel \
python-devel darcs hdf5-devel boost-devel pcre-devel opencv-devel \
libcurl-devel blosc-devel libtiff-devel libjpeg-turbo-devel \
libusbx-devel systemd-devel libraw1394.x86_64 hg libtirpc-devel \
liberation-fonts-common liberation-narrow-fonts \
liberation-mono-fonts liberation-serif-fonts liberation-sans-fonts \
logrotate xorg-x11-fonts-misc cpan kernel-devel symlinks \
dkms procServ curl netcdf netcdf-devel patchelf python3-devel \
boost-devel glib2-devel libtool popt-devel

sudo yum install -y svn python3 graphviz screen patchelf

pip3 install --user pyyaml

