#!/bin/bash

LOG=`date +"mtca_run_ioc_%d-%m-%Y_%H-%M.log"`

cat /dev/null > $LOG

exec &> >(tee -a "$LOG")

#
# Configure IOC
#

    if [ ! -d llrf_v2_digital ]; then
	echo "Clone llrf_v2_digital"
	git clone https://jaroslawszewinski:fojD9EB6WtHuaLmr@bitbucket.org/europeanspallationsource/llrf_v2_digital.git
#	(
#	    cd llrf_v2_digital
#	    #git checkout cfa65c6f334fad235b8832d0dc6a2030507dca66
#	)

#    else
#        echo "Pull llrf-deployment-tools"
#	( cd llrf-deployment-tools && git pull )
    fi



#
# Run LLRF IOC
#

#export EPICS_SRC=`readlink -f e3`
#source $EPICS_SRC/tools/setenv

source /epics/base-7.0.4/require/3.3.0/bin/setE3Env.bash


export EPICS_CA_ADDR_LIST=127.0.0.1
export LLRF_IOC_NAME=LLRF





echo "Starting IOC"

(cd /opt/iocs/llrf && iocsh.bash st.cmd) &

IOC_PID=$!

echo "IOC PID: $IOC_PID"

sleep 10



echo "IOC started"

echo "Installing Python for LLRF"

make -C llrf_v2_digital install_py

echo "Running LLRF Python tests"

python3 llrf_v2_digital/scripts/helpers/llrf_test_setup.py

sleep 1000
