#!/bin/bash

LOG=`date +"mtca_sis8300_firmware_upgarde_%d-%m-%Y_%H-%M.log"`

cat /dev/null > $LOG

exec &> >(tee -a "$LOG")

SEP1=`for i in {1..80}; do echo -n "="; done`
SEP2=`for i in {1..80}; do echo -n "-"; done`


#
# SIS 8300 firmware upgrade
#

LLRF_FW_VER=0.6.0
LLRF_FW_FILE=llrf-$LLRF_FW_VER.bin

export PATH=$PATH:/usr/local/bin

echo $SEP1
echo "SIS8300 Firmware check"
echo $SEP2

SIS_FOUND=0

for PCI_DEV in /sys/bus/pci/devices/*
do
    if [ -d $PCI_DEV/sis8300 ]
    then
	SIS_FOUND=1
	PCI_ID=`echo $PCI_DEV | rev | cut -f1 -d/ | rev`
	DEVICE=/dev/`ls -d $PCI_DEV/sis8300/sis8300-* | rev | cut -f1 -d/ | rev`
	FW_VER=`sudo /usr/local/bin/sis8300drv_fwver $DEVICE`

	echo "Found SIS8300 PCI device $PCI_ID at $DEVICE:"
	echo -e "\t PCI ID: $PCI_ID"
	echo -e "\t Device: $DEVICE"
	echo -e "\t FW Ver: $FW_VER"



	if [ $FW_VER != $LLRF_FW_VER ]
	then
	    echo "Bad firmware version: $FW_VER (expected $LLRF_FW_VER)"
	    echo "Do firmware upgrade? (y/n)"
	    
	    read DO_FW_UPDATE
	
	    if [ $DO_FW_UPDATE == "y" ]; then
		if ! sudo -E /usr/local/bin/sis8300drv_flashfw $DEVICE $LLRF_FW_FILE ; then
		    echo "Flashing memory failed for first time, running second time"
		    if ! sudo -E /usr/local/bin/sis8300drv_flashfw $DEVICE $LLRF_FW_FILE ; then
			echo "Flashing memory failed second time"
			exit 1
		    fi
		fi
		
		echo "Shutting down $PCI_ID PCI Device"
		echo "echo 1 > $PCI_DEV/remove"
		sudo -E bash -c "echo 1 > $PCI_DEV/remove"
		echo "Power-cycle device via MCH now to load FPGA from EEPROM, and press ENTER when ready..."
		read
		echo "echo 1 > /sys/bus/pci/rescan"
		sudo -E bash -c "echo 1 > /sys/bus/pci/rescan"
		
		if [ -d $DEVICE ]; then
		    FW_VER=`sis8300drv_fwver $DEVICE`
		    echo "Firmware version is now $FW_VER"
		else
		    echo "Device $DEVICE is lost"
		    exit 1
		fi
	    else
		echo "Aborted firmware update for $PCI_ID PCI Device"
		sleep 3
		exit 1
	    fi
	else
	    echo "Firmware version is OK ($FW_VER)"
	fi
    fi
done


if [ $SIS_FOUND == 0 ]; then
    echo "No SIS boards found"
fi
