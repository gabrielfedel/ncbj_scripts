#!/bin/bash

#DEBUG=1
STEPFILE=`readlink -f .`/step

STEP=0

if [ -f $STEPFILE ]; then
    STEP=`cat $STEPFILE`
fi

export EPICS_FOLDER=/epics
export EPICS_VER=7.0.4
export E3_REQUIRE_VERSION=3.3.0
export BASEREQVER=$EPICS_VER-$E3_REQUIRE_VERSION
export ASYN_VER=4.37.0
export SEQ_VER=2.2.7
export SSCAN_VER=2.11.3
export CALC_VER=3.7.3
export BUSY_VER=1.7.2_c596e1c
export DEVLIB2_VER=2.9.0

export LOKI_VER=1.1.0
export NDS_VER=2.3.4
export SCALING_VER=1.7.1
export SIS8300DRV_VER=4.9.1
export SIS8300_VER=3.4.0
export SIS8300LLRFDRV_VER=6.0.0
export SIS8300LLRF_VER=7.4.0-3.3.0/5.0.1
export LLRFSYSTEM_VER=3.2.0
export ISLAND_VER=1.3.0

export E3_REPO=https://gitlab.esss.lu.se/e3/e3.git


LOG=`date +"mtca_crate_install_%d-%m-%Y_%H-%M.log"`

cat /dev/null > $LOG

exec &> >(tee -a "$LOG")

export PATH=$PATH:/usr/local/bin

SEP1=`for i in {1..80}; do echo -n "="; done`
SEP2=`for i in {1..80}; do echo -n "-"; done`
NUM=1


function read_eth_macs()
{
    /sbin/ifconfig -a | while read -r LINE
    do
	if [[ $LINE =~ ^[a-z0-9]+: ]]; then
	    IFACE=`echo $LINE | cut -f1 -d:`
        fi

	if [[ $LINE =~ ether ]]; then
	    MAC=`echo $LINE | cut -f2 -d" "`
	    echo $MAC $IFACE
#		echo $LINE
        fi
    done
}


function run()
{
    echo $SEP1
    echo -n "# "; pwd
    echo "# $NUM. $1"
    echo $SEP2

    if [ $NUM -gt $STEP ]; then
        if [ -n "$DEBUG" ]; then
	    echo "Press ENTER to run..."
    	    read
        fi

	time eval $1

	if [ $? -ne 0 ]; then
	    echo $SEP2
	    echo "!!!  $NUM. $1 Failed!"
	    echo $SEP1
	    exit 1
	fi

	STEP=$NUM
        echo $STEP > $STEPFILE
    else
	echo "Already done, skipping"
    fi
    NUM=`expr $NUM + 1`
}

function chdir()
{
    if [ ! -d "$1" ]; then
	echo "No such dir $1"
	exit 1
    fi

    echo "Changing dir to `readlink -f $1`"

    cd "$1"
}

function get_e3_module_specific()
{
    chdir "$1"
    run   "git clone https://gitlab.esss.lu.se/e3/wrappers/$1/e3-$2.git"
    chdir "e3-$2"
    VER=`git tag | grep $BASEREQVER | grep $3 | tail -n 1`
    run   "git checkout $VER"
    run   "make init"
    run   "make patch"
    run   "sudo make -j$4 rebuild"
    chdir "../.."
}


echo -n "Date  : "; date
echo    "Host  : $HOSTNAME"
echo -n "System: "; cat /etc/centos-release
echo -n "Arch  : "; uname -a
echo -n "Disk  : "; lsblk -S -o type,vendor,model,serial
df -h
echo "Ethernet MACs:"; read_eth_macs

#
# EPICS
#

run "sudo mkdir $EPICS_FOLDER"
run "sudo chown $USER $EPICS_FOLDER"

run "git clone $E3_REPO"
chdir "e3"
run "bash e3_building_config.bash -y -t $EPICS_FOLDER -b $EPICS_VER -r $E3_REQUIRE_VERSION setup"
run "bash e3.bash base"

run "bash e3.bash req"

# core modules

run   "mkdir -p core"

get_e3_module_specific "core" "asyn" $ASYN_VER 4
get_e3_module_specific "core" "seq" $SEQ_VER 1
get_e3_module_specific "core" "sscan" $SSCAN_VER 4
get_e3_module_specific "core" "calc" $CALC_VER 4
get_e3_module_specific "core" "busy" $BUSY_VER 4
get_e3_module_specific "core" "devlib2" $DEVLIB2_VER 4

run   "mkdir -p rf"

# LLRF modules

get_e3_module_specific "rf" "loki" $LOKI_VER 4
get_e3_module_specific "rf" "nds" $NDS_VER 4
get_e3_module_specific "rf" "scaling" $SCALING_VER 4
get_e3_module_specific "rf" "sis8300drv" $SIS8300DRV_VER 4
get_e3_module_specific "rf" "sis8300" $SIS8300_VER 4
get_e3_module_specific "rf" "sis8300llrfdrv" $SIS8300LLRFDRV_VER 4

chdir "rf"
run   "git clone https://gitlab.esss.lu.se/e3/wrappers/rf/e3-sis8300llrf.git"
chdir "e3-sis8300llrf"
run   "git checkout $SIS8300LLRF_VER"
run   "make init"
run   "make patch"
run   "sudo make -j$4 rebuild"
chdir "../.."

get_e3_module_specific "rf" "llrfsystem" $LLRSYSTEM_VER 4
get_e3_module_specific "rf" "evr-island" $ISLAND_VER 4

# timing modules

run   "mkdir -p rf"

chdir "rf"
run   "git clone https://gitlab.esss.lu.se/gabrielfedel/e3-mrfioc2-copy.git"
chdir "e3-mrfioc2-copy"
run   "make init"
run   "make patch"
run   "sudo make -j$4 rebuild"
chdir "../.."

export EPICS_SRC=`readlink -f .`
source $EPICS_SRC/tools/setenv
export EPICS_CA_ADDR_LIST=127.0.0.1


chdir ".."
run   "git clone https://gitlab.esss.lu.se/icshwi/llrf-deployment-tools.git"
#
#
#
chdir "$EPICS_SRC/rf/e3-sis8300drv"
run "make tools"
#
run "sudo make dkms_add"
run "sudo make dkms_build"
run "sudo make dkms_install"

# Install IOC
chdir "../../.."
run   "sudo mkdir -p /opt/iocs"
run   "sudo chown $USER /opt/iocs"
run   "git clone https://gitlab.esss.lu.se/ioc/e3-ioc-llrf-generic.git /opt/iocs/llrf"

#
chdir "llrf-deployment-tools"

run   "bash deployCLI.sh"
echo "here"
echo "$EPICS_SRC $EPICS_BASE $EPICS_HOST_ARCH"
run   "bash exposeSO.sh"
run   "bash deployServiceIOC.sh"
#chdir ".."
#
#echo Finished
